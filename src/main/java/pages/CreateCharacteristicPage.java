package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage extends HomePage{
    public CreateCharacteristicPage(WebDriver driver)  {super(driver);}

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecificationLimitTxt;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecificationLimitTxt;

    @FindBy(css = "input[value='Create']")
    private WebElement createBtn;

    @FindBy(css = "span[class='text-danger field-validation-error']")
    private WebElement selectProcessError;

    @FindBy(css = "a[class='btn btn-primary']")
    private WebElement backToListBtn;

    public CreateCharacteristicPage typeName(String nameInputText){
        nameTxt.clear();
        nameTxt.sendKeys(nameInputText);
        return this;
    }

    public CreateCharacteristicPage typeLsl(String lslInputText){
        lowerSpecificationLimitTxt.clear();
        lowerSpecificationLimitTxt.sendKeys(lslInputText);
        return this;
    }

    public CreateCharacteristicPage typeUsl(String uslInputText){
        upperSpecificationLimitTxt.clear();
        upperSpecificationLimitTxt.sendKeys(uslInputText);
        return this;
    }

    public CreateCharacteristicPage selectProcess(String processName){
        new Select(projectSlc).selectByVisibleText(processName);
        return this;
    }

    public CreateCharacteristicPage submitCreateWithFailure(){
        createBtn.click();
        return this;
    }

    public CreateCharacteristicPage assertProcessError(String expectedMessage){
        Assert.assertEquals(selectProcessError.getText(), expectedMessage);
        return this;
    }


    public CharacteristicsPage submitCreate(){
        createBtn.click();
        return new CharacteristicsPage(driver);
    }

    public CharacteristicsPage backToList(){
        backToListBtn.click();
        return new CharacteristicsPage(driver);
    }
}
