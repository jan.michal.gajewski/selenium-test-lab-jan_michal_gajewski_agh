package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessPage extends HomePage{

    public CreateProcessPage(WebDriver driver){
        super(driver);
    }

    @FindBy(css = "#Name")
    private WebElement processNameInput;

    public CreateProcessPage typeName(String processName){
        processNameInput.sendKeys(processName);
        return this;
    }

    @FindBy(css = "input[type='submit']")
    private WebElement createProcessBtn;


    public ProcessesPage submitCreate() {
        createProcessBtn.click();
        return new ProcessesPage(driver);
    }

    public CreateProcessPage submitCreateWithFailure() {
        createProcessBtn.click();
        return this;
    }

    @FindBy(css = "[data-valmsg-for=Name]")
    private WebElement nameErrorMessage;

    public CreateProcessPage assertProcessNameError(String expErrorMessage) {
        Assert.assertEquals(nameErrorMessage.getText(),expErrorMessage);
        return this;
    }

    @FindBy(css = ".btn[href$='/Projects']")
    private WebElement backToListBtn;

    public ProcessesPage backToList(){
        backToListBtn.click();
        return new ProcessesPage(driver);
    }
}
