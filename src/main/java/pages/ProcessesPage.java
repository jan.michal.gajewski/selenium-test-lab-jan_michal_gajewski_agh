package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.UUID;

public class ProcessesPage extends HomePage{

    public ProcessesPage(WebDriver driver){
        super(driver);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement processPageHeader;

    @FindBy(css = "a[href$='/Projects/Create']")
    private WebElement addProcessBtn;


    public ProcessesPage assertProcessesUrl(String actualUrl){
        Assert.assertEquals(driver.getCurrentUrl(), actualUrl);
        return this;
    }

    public ProcessesPage assertProcessesHeader(){
        Assert.assertEquals(processPageHeader.getText(), "Processes");
        return this;
    }


    private String GENERIC_PROCESS_ROW_PATH = "//td[text()='%s']/..";

    public ProcessesPage assertProcess(String expName, String expDescription, String expNotes){
        String ProcessXpath = String.format(GENERIC_PROCESS_ROW_PATH, expName);

        WebElement processRow = driver.findElement(By.xpath(ProcessXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription,expDescription);
        Assert.assertEquals(actNotes, expNotes);

        return this;
    }

    public CreateProcessPage clickAddProcess() {
        addProcessBtn.click();
        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertProcessIsNotShown(String processName){
        String processXpath = String.format(GENERIC_PROCESS_ROW_PATH, processName);
        List <WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }

}
