package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateResultsPage extends HomePage{

    public CreateResultsPage(WebDriver driver) {super(driver);}

    @FindBy(css = "#Sample")
    private WebElement sampleTxt;

    @FindBy(css = "#Values")
    private WebElement resultsTxt;

    @FindBy(css = "input[value='Create']")
    private WebElement createBtn;

    public CreateResultsPage typeSampleName(String sampleNameText){
        sampleTxt.clear();
        sampleTxt.sendKeys(sampleNameText);
        return this;
    }

    public CreateResultsPage typeResults(String resultsText){
        resultsTxt.clear();
        resultsTxt.sendKeys(resultsText);
        return this;
    }

    public ResultsPage submitCreate(){
        createBtn.click();
        return new ResultsPage(driver);
    }

}
