package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashBoardsPage extends HomePage {

    public DashBoardsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "body > div > div > div.right_col > div > h3")
    private WebElement dashboardEmptyListMessage;

    @FindBy(css = "x_panel tile overflow_hidden")
    private WebElement dashboardPageHeader;

    public DashBoardsPage assertDashboardUrl(String actualUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), actualUrl);
        return this;
    }

    public boolean isElementPresent(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException _e) {
            return false;
        }
    }

    public DashBoardsPage assertDemoProjectIsShown() {
        Assert.assertTrue(isElementPresent(dashboardPageHeader) || isElementPresent(dashboardEmptyListMessage));
        return this;
    }
}