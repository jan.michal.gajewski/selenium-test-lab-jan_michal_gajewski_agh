package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultsPage extends HomePage{

    public ResultsPage(WebDriver driver) {super(driver);}

    @FindBy(css = "a[class='btn btn-success']")
    private WebElement addResultSampleBtn;

    @FindBy(css = "a[class='btn btn-default']")
    private WebElement backToCharacteristicsBtn;


    public CreateResultsPage clickAddResults(){
        addResultSampleBtn.click();
        return new CreateResultsPage(driver);
    }

    public CharacteristicsPage backToCharacteristics(){
        backToCharacteristicsBtn.click();
        return new CharacteristicsPage(driver);
    }

}

