package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.testng.Assert;

import java.util.List;

public class CharacteristicsPage extends HomePage{

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement characteristicsPageHeader;

    @FindBy(css = "a[href$='/Characteristics/Create']")
    private WebElement AddNewCharacteristicBtn;



    public CreateCharacteristicPage clickAddCharacteristics(){
        AddNewCharacteristicBtn.click();
        return new CreateCharacteristicPage(driver);
    }


    public CharacteristicsPage assertCharacteristicsUrl(String actualUrl){
        Assert.assertEquals(driver.getCurrentUrl(), actualUrl);
        return this;
    }

    public CharacteristicsPage assertCharacteristicsPage(){
        Assert.assertEquals(characteristicsPageHeader.getText(), "Characteristics");
        return this;
    }

    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[text()='%s']/..";

    public CharacteristicsPage assertCharacteristics(String expName, String expLsl, String expUsl, String expBinCount){
        String characteristicsXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        WebElement characteristicsRow = driver.findElement(By.xpath(characteristicsXpath));

        String actLsl = characteristicsRow.findElement(By.xpath("./td[3]")).getText();
        String actUsl = characteristicsRow.findElement(By.xpath("./td[4]")).getText();
        String actBinCount = characteristicsRow.findElement(By.xpath("./td[5]")).getText();

        Assert.assertEquals(actLsl, expLsl);
        Assert.assertEquals(actUsl, expUsl);
        Assert.assertEquals(actBinCount, expBinCount);

        return this;
    }

    public CharacteristicsPage assertCharacteristicsIsNotShown(String expName){
        String processXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        List <WebElement> characteristics = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(characteristics.size(), 0);
        return this;
    }

    private String GENERIC_CHARACTERISTIC_RESULTS_XPATH =
            "//td[text()='%s']/..//a[contains(@href, 'Results')]";

    public ResultsPage goToResults(String characteristicName){
        String resultsBtnXpath = String.format(GENERIC_CHARACTERISTIC_RESULTS_XPATH, characteristicName);
        driver.findElement(By.xpath(resultsBtnXpath)).click();
        return new ResultsPage(driver);
    }

    private String GENERIC_CHARACTERISTIC_REPORT_XPATH =
            "//td[text()='%s']/..//a[contains(@href, 'Report')]";

    public ReportPage goToReport(String characteristicName){
        String reportBtnXpath = String.format(GENERIC_CHARACTERISTIC_REPORT_XPATH, characteristicName);
        driver.findElement(By.xpath(reportBtnXpath));
        return new ReportPage(driver);
    }
}

