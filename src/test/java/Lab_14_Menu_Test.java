import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Menu_Test extends SeleniumBaseTest{

    @Test
    public void menuTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                    .assertProcessesUrl("http://localhost:4444/Projects")
                    .assertProcessesHeader()
                .goToCharacteristics()
                    .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                    .assertCharacteristicsPage()
                .goToDashboards()
                    .assertDashboardUrl("http://localhost:4444/")
                    .assertDemoProjectIsShown();
    }
}
