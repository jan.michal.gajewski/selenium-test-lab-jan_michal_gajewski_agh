import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;

public class Lab_6_Test_Niepoprawnego_Logowania_Haslo_Test {

    @Test
    public void incorrectLoginTestWrongPassword() {
        System.setProperty("webdriver.chrome.driver",
                "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");
        WebElement emailTxt = driver.findElement(By.cssSelector("#Password"));
        emailTxt.sendKeys("incorrectPass");
        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();
        loginBtn.click();
        //WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
        //Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
        boolean doesErrorExists = false;

        for (int i = 0; i < validationErrors.size(); i++) {
            if (validationErrors.get(i).getText().equals("The Email field is required.")) {
                doesErrorExists = true;
                break;
            }
        }
        Assert.assertTrue(doesErrorExists);
        driver.quit();
    }
}
