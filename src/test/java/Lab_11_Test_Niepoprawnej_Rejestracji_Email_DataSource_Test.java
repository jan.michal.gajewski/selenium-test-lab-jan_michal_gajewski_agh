import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

public class Lab_11_Test_Niepoprawnej_Rejestracji_Email_DataSource_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmails) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.registerNewUser();
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.typeEmail(wrongEmails);
        createAccountPage.submitRegisterWithFailure();
        createAccountPage.expectedEmailError("The Email field is not a valid e-mail address.");
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTestWithChaining(String wrongEmails) {
        new LoginPage(driver)
                .registerNewUser()
                .typeEmail(wrongEmails)
                .submitRegisterWithFailure()
                .expectedEmailError("The Email field is not a valid e-mail address.");
    }
}
